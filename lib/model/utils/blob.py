# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Blob helper functions."""
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
# from scipy.misc import imread, imresize
import cv2

try:
    xrange          # Python 2
except NameError:
    xrange = range  # Python 3


def im_list_to_blob(ims):
    """Convert a list of images into a network input.

    Assumes images are already prepared (means subtracted, BGR order, ...).
    """
    max_shape = np.array([im.shape for im in ims]).max(axis=0)
    num_images = len(ims)
    blob = np.zeros((num_images, max_shape[0], max_shape[1], 3),
                    dtype=np.float32)
    for i in xrange(num_images):
        im = ims[i]
        blob[i, 0:im.shape[0], 0:im.shape[1], :] = im

    return blob

def prep_im_for_blob(im, pixel_means, target_size, max_size):
    """Mean subtract and scale an image for use in a blob."""
    
    # img = deepcopy(im)
    # print(pixel_means)
    im = im.astype(np.float32, copy=False)
    # pixel_means = pixel_means.astype(np.uint8)
    # temp = im.transpose(2,0,1)
    # img_mean = np.mean(temp[0].flatten()).astype(np.uint8)
    img_pixel_mean = np.array([[[0.0, 0.0, 0.0]]])

    im -= pixel_means
    # im -= img_pixel_mean

    # print(im.shape,pixel_means.shape)

    im_shape = im.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])
    im_scale = float(target_size) / float(im_size_min) # im_scale is 1.0 for all images 
    # print(im_scale)
    
    # im = cv2.resize(im, None, None, fx=im_scale, fy=im_scale,
    #                 interpolation=cv2.INTER_LINEAR)
    
    # im = img/255.0

    # temp_img = img
    # plt.imshow(temp_img)
    # plt.show()  

    # out = im
    # temp_img = out
    # plt.imshow(temp_img)
    # plt.show()  

    # im = cv2.resize(im, None, None, fx=im_scale, fy=im_scale,
    #                 interpolation=cv2.INTER_LINEAR)

    # out = im
    # temp_img = out
    # plt.imshow(temp_img)
    # plt.show()  

    # x = im/255.0

    return im, im_scale
